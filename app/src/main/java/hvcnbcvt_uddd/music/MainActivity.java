package hvcnbcvt_uddd.music;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ImageButton btnNext, btnPrev, btnStop, btnPlay;
    TextView tvSong, tvTimeSong, tvTimeTotal;
    SeekBar seekBar;
    ArrayList<Song> arraySong;
    int position = 0;
    MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Control();
        AddSong();
        CreatMediaplayer();
        Event();
    }

    private void CreatMediaplayer() {
        mediaPlayer = MediaPlayer.create(MainActivity.this,arraySong.get(position).getFile());
        tvSong.setText(arraySong.get(position).getName());
    }

    private void Event() {

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mediaPlayer.isPlaying())
                    {
                        //play -> pause
                        btnPlay.setImageResource(R.drawable.if_play_51518);
                        mediaPlayer.pause();
                    }
                    else
                        {
                            //pause -> play
                             btnPlay.setImageResource(R.drawable.if_pause_51539);
                             mediaPlayer.start();
                        }
                        setTimeTatol();
                        updateTimeSong();

            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.stop();
                mediaPlayer.release();
                btnPlay.setImageResource(R.drawable.if_play_51518);
                CreatMediaplayer();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position++;
                if(position>arraySong.size()-1)
                {
                    position = 0;
                }
                if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.stop();
                }
                CreatMediaplayer();
                mediaPlayer.start();
                btnPlay.setImageResource(R.drawable.if_pause_51539);
                setTimeTatol();
                updateTimeSong();
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position--;
                if(position<0)
                {
                    position = arraySong.size()-1;
                }
                if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.stop();
                }
                CreatMediaplayer();
                mediaPlayer.start();
                btnPlay.setImageResource(R.drawable.if_pause_51539);
                setTimeTatol();
                updateTimeSong();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                // kéo đâu thay đổi đến đó
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // chạm vào cập nhật
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // di chuyển xong mới cập nhật
                    mediaPlayer.seekTo(seekBar.getProgress());
            }
        });
    }

    private void updateTimeSong()
    {
        //Handler update tiến trình bao lâu 1 lần
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SimpleDateFormat dinhDangGio = new SimpleDateFormat("mm:ss");
                tvTimeSong.setText(dinhDangGio.format(mediaPlayer.getCurrentPosition()));
                //getCurrentPosition lấy vị trí hiện tại của media

                //update project của seekBar
                seekBar.setProgress(mediaPlayer.getCurrentPosition());

                //kiểm tra thời gian bài hát nếu kết thúc -> btnNext
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        position++;
                        if(position>arraySong.size()-1)
                        {
                            position = 0;
                        }
                        if(mediaPlayer.isPlaying())
                        {
                            mediaPlayer.stop();
                        }
                        CreatMediaplayer();
                        mediaPlayer.start();
                        btnPlay.setImageResource(R.drawable.if_pause_51539);
                        setTimeTatol();
                        updateTimeSong();
                    }
                });
                //update 0.5s/lần
                handler.postDelayed(this,500);
            }
        },100);
    }

    private void setTimeTatol()
    {
        //Chuyển sang phút giây
        SimpleDateFormat dinhDangGio = new SimpleDateFormat("mm:ss");

        //hàm getDuaration sẽ chuyển sang mili giây -> phải chyển sang phút giây
        tvTimeTotal.setText(dinhDangGio.format(mediaPlayer.getDuration()));

        //gán max của seekBar = mediaplayer.getDuaration();
        seekBar.setMax(mediaPlayer.getDuration());
    }

    private void AddSong() {
        arraySong = new ArrayList<>();
        arraySong.add(new Song("Vài lần đón đưa",R.raw.vai_lan_don_dua));
        arraySong.add(new Song("Về phía mưa",R.raw.ve_phia_mua));
        arraySong.add(new Song("Xin đừng lặng im",R.raw.xin_dung_lang_im));
        arraySong.add(new Song("Vội vàng",R.raw.voi_vang));
        arraySong.add(new Song("Yêu là tha thứ",R.raw.yeu_la_tha_thu));
    }

    private void Control() {
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        btnPrev = (ImageButton) findViewById(R.id.btnPrev);
        btnStop = (ImageButton) findViewById(R.id.btnStop);
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        tvSong = (TextView) findViewById(R.id.tvSong);
        tvTimeSong = (TextView) findViewById(R.id.tvTimeSong);
        tvTimeTotal = (TextView) findViewById(R.id.tvTimeTotal);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
    }
}
